package com.ldap.java.test;

import java.util.Hashtable;

import javax.naming.Context;

public final class Utils {

	public static final String CTX = "com.sun.jndi.ldap.LdapCtxFactory";

	public static final String USR = "lbonaldo";

	public static final String PWD = "Elaidemo1";

	public static final String URL = "ldap://ElaidePDC.elaidedev.local";

	public static final String AUT = "DIGEST-MD5";

	public static final Hashtable<String, String> ENV = new Hashtable<>();

	static {
		ENV.put(Context.INITIAL_CONTEXT_FACTORY, CTX);
		ENV.put(Context.PROVIDER_URL, URL);
		ENV.put(Context.SECURITY_AUTHENTICATION, AUT);
		ENV.put(Context.SECURITY_PRINCIPAL, USR);
		ENV.put(Context.SECURITY_CREDENTIALS, PWD);
	};

	public static void log(String log) {
		System.out.println(log);
	}

	public static void err(String err) {
		System.err.println(err);
	}
	
	public static void err(String err, Error e) {
		System.err.println(err);
		if (e != null)
			e.printStackTrace();
	}
	
	public static void err(String err, Exception ex) {
		System.err.println(err);
		if (ex != null)
			ex.printStackTrace();
	}
}
