package com.ldap.java.test;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.junit.Test;

public class Login {

	@Test
	public void test() {
		try {

			Utils.log("autenticazione in corso...");
			DirContext ctx = new InitialDirContext(Utils.ENV);
			Utils.log("autenticato");

			ctx.close();

		} catch (CommunicationException ce) {
			Utils.err("Errore durante la comunicazione con il servizio di autenticazione remota", ce);
		} catch (AuthenticationException ae) {
			Utils.err("Non esiste nessun utente con queste credenziali", ae);
		} catch (Exception ne) {
			Utils.err("Errore generico", ne);
		} catch (Error e) {
			Utils.err("Errore grave", e);
		} finally {
			Utils.log("Fine test");
		}
	}
}
